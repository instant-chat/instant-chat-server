const { name: appName } = require('../package.json');

module.exports = {
  createClient(redisUrl) {
    const URL = require('url');
    const Redis = require('redis');
    return new Promise(function(resolve, reject) {
      if (!redisUrl) return resolve(Redis.createClient());
      let { auth, hostname, port } = URL.parse(redisUrl);
      let redis = Redis.createClient(port, hostname);
      if (!auth) return resolve(redis);
      let [, password] = auth.split(':');
      redis.auth(password, function(err) {
        if (err) return reject(err);
        resolve(redis);
      });
    });
  },

  clientIDFor(link) {
    return link && `${appName}:client:${link}`;
  },

  secretIDFor(uuid) {
    return uuid && `${appName}:secret:${uuid}`;
  },

  revisionIDFor(revision) {
    return revision && `${appName}:index:${revision}`;
  }
};
