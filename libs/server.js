const http = require('http');
const Koa = require('koa');
const morgan = require('koa-morgan');
const bodyParser = require('koa-bodyparser');
const betterErrorHandler = require('koa-better-error-handler');
const setVersionHeaders = require('koa-verto');
const koa404Handler = require('koa-404-handler');
const clientsRouter = require('../routes/clients');
const secretsRouter = require('../routes/secrets');
const emberRouter = require('../routes/ember');
const attachChatSocket = require('./chat-socket');

class Server {
  constructor(redis, logger = morgan('combined')) {
    this.redis = redis;
    this.app = new Koa();
    this.app.context.api = true;
    this.app.context.onerror = betterErrorHandler;
    this.app
      .use(logger)
      .use(bodyParser())
      .use(setVersionHeaders())
      .use(clientsRouter(this.redis).routes())
      .use(secretsRouter(this.redis).routes())
      .use(emberRouter(this.redis).routes())
      .use(koa404Handler);
    this.server = http.createServer(this.app.callback());
    this.io = require('socket.io')(this.server);
    attachChatSocket(this.io);
  }

  listen() {
    return this.server.listen(...arguments);
  }

  close() {
    this.io.close();
    this.server.close();
  }
}

module.exports = Server;
