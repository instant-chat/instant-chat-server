const Server = require('./libs/server');
const createRedisClient = require('./libs/redis-utils').createClient;

const PORT = process.env.PORT || 3000;
const REDIS_URL = process.env.REDIS_URL || process.env.REDISTOGO_URL;

createRedisClient(REDIS_URL)
  .then(function(redis) {
    new Server(redis).listen(PORT);
  })
  .catch(function(error) {
    console.error(error);
  });
