# Instant Chat Server

This is a very simplistic Socket.IO server.

## Recieved Events

| `register-client-info` | Registers client's publicKey, clientName, and roomName then assigns a temporary link. |
| `request-client-info`  | Query server for the publicKey, clientName, and roomName associated with a link. |
| `join-room`            | Join the roomName socket.io room |
| `leave-room`           | Leave the roomName socket.io room |
| `post-messages`        | Send a message to all clients in a room. |

## Sent Events

| `address-assignment` | All clients recive this event on connection. Payload will contain the unique client address. |
| `link-assignment`    | Response to a register-client-info event. Payload will contain the temporary link. |
| `client-info`        | Response to a request-client-info. Payload will contain the publicKey, clientName, and roomName. |
| `link-expired`       | Tells the client that its temporary link has expired and needs to redo a register-key event. |
| `messages`           | Tells the client that it recieved a message. Payload has a message type and some encrypted ciphertext. |
| `client-error`       | Tells the client that their previous request event failed. Payload will have the error message. |

## Example Communication

1.  Alice connects to Server
2.  Server sends Alice a `address-assignment` event with payload `{ address: 'aliceAddress' }`
4.  Alice sends Server a `register-client-info` event with payload `{ publicKey: 'bada55', room: 'aliceAddress' }`
5.  Server sends Alice a `link-assignment` event with payload `{ link: 'LINK' }`
6.  Bob connects to Server
7.  Server sends Bob a `address-assignment` event with payload `{ address: 'bobAddress' }`
8.  Bob sends Server a `request-client-info` event with payload `{ link: 'LINK' }`
9.  Server sends Bob a `client-info` event with payload `{ publicKey: 'bada55', room: 'aliceAddress', address: 'aliceAddress' }`
10. Bob sends Server a `join-room` event with payload `{ room: 'aliceAddress' }`
11. Bob sends Server a `post-messages` event with payload `{ type: 'message', clients: { aliceAddress: 'ciphertext' } }`
12. Server sends everyone in the `aliceAddress` room the `messages` event with payload `{ from: 'bobAddress', type: 'message', clients: { aliceAddress: 'ciphertext' } }`
