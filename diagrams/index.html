<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>instant-chat cryptographic and interchange overviews</title>
  <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
  <!-- <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css"> -->
  <!-- <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css"> -->
  <style>
    section.route {
      display: none;
    }
    section.route.current {
      display: block;
    }
    footer a.route.active {
      font-weight: bold;
    }
    .navbar-section a.route.active {
      border-bottom: 2px solid #302ecd;
    }
    .big-text-clear-btn {
      position: absolute;
      top: 0;
      right: 0;
    }
    .big-text-wrapper {
      align-items: center;
      background: black;
      border-radius: 6px;
      color: white;
      display: flex;
      justify-content: center;
      min-height: 500px;
    }
    .big-text {
      font-family: sans-serif;
      font-size: 72pt;
      text-align: center;
      font-weight: bold;
    }
    @media print {
      section.route {
        display: block;
      }
      .navbar {
        display: none;
      }
    }
    .Constant { color: #008080; }
    .Statement { color: #008000; }
  </style>
</head>
<body>
  <div class="container grid-lg">
    <header class="navbar">
      <section class="navbar-section">
        <a class="navbar-brand text-bold mr-2 route" href="#index">instant-chat</a>
        <a class="btn btn-link route" href="#chat-protocol">Chat Protocol</a>
        <a class="btn btn-link route" href="#client-crypto">Client Crypto</a>
        <a class="btn btn-link route" href="#big-text">Big Text</a>
        <a class="btn btn-link route" href="#one-time-secret">One Time Secret</a>
      </section>
    </header>

    <section id="index" class="route">
      <h1>Overview</h1>
      <p>This is basic documentation of the internal systems of the <code>instant-chat</code> system.<p>
      <p>This system is a method by which multiple users can share textual information with each other without any prior knowledge of each other.</p>
      <p>The use case is targeted to the following:</p>
      <pre class="code" data-lang="gherkin"><code>
      Scenario: Bob wishes to communicate with Alice
        GIVEN Alice knows nothing about Bob
        WHEN Bob goes to the instant-chat app
        AND presents Alice a scan-able/transcribe-able URL
        THEN the two can chat between each other
        AND the communication is encrypted between the two

      Scenario: Charlie wishes to join the conversation
        GIVEN Bob and Alice have established secure communications
        WHEN Bob or Alice present to Charlie a scan-able/transcribe-able URL
        THEN the three can chat between each other
        AND the communication is encrypted between everyone

      Scenario: Bob, Alice, and Charlie are chatting on instant-chat
        GIVEN Bob, Alice, or Charlie wish to end the conversation
        WHEN everyone leaves the app
        THEN there is no plain text record of the conversation
      </code></pre>
      <p>The target audience would be the deaf and hard of hearing.</p>
      <p>Any section marked as <span class="badge" data-badge="Experimental"></span> are not part of the chat but are features I would like to have available in the client apps. They will provide value to users but is not at all associated with the chat system and may have their own crypto requirements.</p>
    </section>

    <section id="chat-protocol" class="route">
      <h1>Chat Protocol</h1>
      <p>This describes the process by which the clients communicate to each other. It shows the initial handshake to trade encryption keys between clients and how it proxies messages between them.</p>
      <p>The initial handshake can be public as their is no sensitive information being sent over the wire. All sensitive data will be properly encrypted as part of the payload of the <code>message</code> events. See the <a href="#client-crypto">Client Crypto</a> section for more details on the cryptography process for <em>messages</em>.</p>

      <h2>Socket.io API</h2>

      <h4><code>address-assignment</code></h4>
      <p>When a client first connects to the Socket.io server they will <strong>receive</strong> a <code>address-assignemnt</code> event.</p>
      <p>Socket.io already assigns a unique ID to every client. We can simply use that.</p>
      <pre class="code" data-lang="JSON"><code>
      <span class="Statement">{</span>
        <span class="Constant">&quot;address&quot;</span>: <span class="Constant">&quot;client0-uuid&quot;</span>
      <span class="Statement">}</span>
      </code></pre>

      <h4><code>join</code></h4>
      <p>When a client wishes to subscribe to a rooms chatter they can <strong>send</strong> the <code>join</code> event.</p>
      <pre class="code" data-lang="JSON"><code>
      <span class="Statement">{</span>
        <span class="Constant">&quot;room&quot;</span>: <span class="Constant">&quot;client0-uuid&quot;</span>
      <span class="Statement">}</span>
      </code></pre>

      <h4><code>message</code></h4>
      <p>Any messages are broadcast to the room that a client has joined. The <code>message</code> is both <strong>sent</strong> and <strong>received</strong>.</p>
      <p><em>This event is designed to contain encrypted data.</em><p>
      <pre class="code" data-lang="JSON"><code>
      <span class="Statement">{</span>
        <span class="Constant">&quot;type&quot;</span>: <span class="Constant">&lt;code&gt;</span>,
        <span class="Constant">&quot;from&quot;</span>: <span class="Constant">&quot;client0-uuid&quot;</span>,
        <span class="Constant">&quot;xdata&quot;</span>: <span class="Statement">{</span>
          <span class="Constant">&quot;client1-uuid&quot;</span>: <span class="Constant">&quot;ciphertext&quot;</span>,
          <span class="Constant">&quot;client2-uuid&quot;</span>: <span class="Constant">&quot;ciphertext&quot;</span>,
          <span class="Constant">&quot;client3-uuid&quot;</span>: <span class="Constant">&quot;ciphertext&quot;</span>,
          <span class="Constant">&quot;client4-uuid&quot;</span>: <span class="Constant">&quot;ciphertext&quot;</span>,
          ...
        <span class="Statement">}</span>
      <span class="Statement">}</span>
      </code></pre>

      <h2>HTTP API</h2>

      <p>All <abbr>HTTP</abbr> requests are expected to have a <code>Content-Type</code> of <code>application/json</code>.</p>

      <h4><code>POST /clients</code></h4>
      <p>Register for a unique link. A link is four uppercase letters. Four was chosen to balance a large enough entropy space to be unique enough for our needs and small enough to easily transcribe, speak aloud, or produce smaller QR Codes.</p>
      <p>Registered links have an expiration of <strong>five minutes</strong>.</p>
      <pre class="code" data-lang="HTTP"><code>
      &rArr; POST /clients
      <span class="Statement">{</span>
        <span class="Constant">&quot;room&quot;</span>: <span class="Constant">&quot;client0-uuid&quot;</span>,
        <span class="Constant">&quot;address&quot;</span>: <span class="Constant">&quot;client0-uuid&quot;</span>,
        <span class="Constant">&quot;pubKey&quot;</span>: <span class="Constant">&quot;ephemeral-public-key&quot;</span>
      <span class="Statement">}</span>

      &lArr; 201 Created
      <span class="Statement">{</span>
        <span class="Constant">&quot;link&quot;</span>: <span class="Constant">&quot;ABCD&quot;</span>
      <span class="Statement">}</span>
      </code></pre>

      <h4><code>GET /clients/:link</code></h4>
      <p>Note that rooms are shared among clients where the address is unique to all clients so it is possible to have these two differ when someone other then the first client offers a join request to another client.</p>

      <pre class="code" data-lang="HTTP"><code>
      &rArr; GET /clients/ABCD

      &lArr; 200 Ok
      <span class="Statement">{</span>
        <span class="Constant">&quot;room&quot;</span>: <span class="Constant">&quot;client0-uuid&quot;</span>,
        <span class="Constant">&quot;address&quot;</span>: <span class="Constant">&quot;client0-uuid&quot;</span>,
        <span class="Constant">&quot;pubKey&quot;</span>: <span class="Constant">&quot;ephemeral-public-key&quot;</span>
      <span class="Statement">}</span>

      &lArr; 404 Not Found
      <span class="Statement">{</span>
        <span class="Constant">&quot;error&quot;</span>: <span class="Constant">&quot;Link has expired&quot;</span>
      <span class="Statement">}</span>
      </code></pre>

      <h2>Sequence Diagram</h2>
      <figure class="figure">
        <img class="img-responsive" src="output/chat-handshake.svg">
      </figure>
    </section>

    <section id="client-crypto" class="route">
      <h1>Client Cryptography Details</h1>

      <p>All crypto is handled client side using <a href="https://download.libsodium.org/doc/">libsodium</a> via the <a href="https://www.npmjs.com/package/libsodium-wrappers">libsodium-wrappers</a> library. Any crypto referenced here will be in terms of libsodium's API.</p>

      <p>All encrypted communication is transmitted via the Socket.io <code>message</code> event described in <a href="#chat-protocol">Chat Protocol</a>. Because the intended client framework is <a href="https://emberjs.com">Ember.js</a> it makes sense that the schema for the encrypted payloads be <a href="https://jsonapi.org">JSON:API</a></p>

      <p>On initial registration the ephemeral keys are generated by <code>crypto_box_keypair</code>. See <a href="chat-protocol">Chat Protocol</a> for deatils on the initial registration process.</p>

      <p>Once a client has recieved the originators ephemeral public key they can then begin to communicate with the following <code>message</code> types.</p>

      <h2>Message Types</h2>

      <p>Every message transferred has a type. There are three base message types: <code>EXCHANGE</code>, <code>CHALLENGE</code>, and <code>DATA</code>.

      <h3><code>EXCHANGE</code> Type</h3>
      <p>This message is meant to initiate a key exchange process.</p>

      <h4>Payload</h4>
      <p>
        <table class="table">
          <tbody>
            <tr>
              <th>Exchange Key</th>
              <td><code>crypto_kx_keypair()</code></td>
            </tr>
            <tr>
              <th>Authorization Nonce</th>
              <td><code>randombytes_buf(crypto_box_NONCEBYTES)</code></td>
            </tr>
            <tr>
              <th>Ciphertext</th>
              <td><code>crypto_box_seal(data, ephemeralPublicKey)</code></td>
            </tr>
            <tr>
              <th>Payload</th>
              <td><code>ciphertext</code></td>
            </tr>
          </tbody>
        </table>
      </p>

      <h4>Data</h4>
      <h6>Example exchange payload</h6>
      <pre class="code" data-lang="JSON:API"><code>
      <span class="Statement">{</span>
        <span class="Constant">&quot;data&quot;</span>: <span class="Statement">{</span>
          <span class="Constant">&quot;id&quot;</span>: <span class="Constant">&quot;client1-address&quot;</span>,
          <span class="Constant">&quot;type&quot;</span>: <span class="Constant">&quot;clients&quot;</span>,
          <span class="Constant">&quot;attributes&quot;</span>: <span class="Statement">{</span>
            <span class="Constant">&quot;name&quot;</span>: <span class="Constant">&quot;Alice&quot;</span>,
            <span class="Constant">&quot;exchangeKeys&quot;</span>: <span class="Statement">{</span>
              <span class="Constant">&quot;publicKey&quot;</span>: <span class="Constant">&quot;bada55&quot;</span>
            <span class="Statement">}</span>
          <span class="Statement">}</span>
        <span class="Statement">}</span>,
        <span class="Constant">&quot;meta&quot;</span>: <span class="Statement">{</span>
          <span class="Constant">&quot;authNonce&quot;</span>: <span class="Constant">&quot;bada55&quot;</span>
        <span class="Statement">}</span>
      <span class="Statement">}</span>
      </code></pre>

      <h6>Constructed Socket.io payload</h6>
      <pre class="code" data-lang="JSON"><code>
      <span class="Statement">{</span>
        <span class="Constant">&quot;type&quot;</span>: <span class="Constant">&quot;exchange&quot;</span>,
        <span class="Constant">&quot;from&quot;</span>: <span class="Constant">&quot;client1-address&quot;</span>,
        <span class="Constant">&quot;xdata&quot;</span>: <span class="Statement">{</span>
          <span class="Constant">&quot;client0-address&quot;</span>: <span class="Constant">&quot;encrypted payload&quot;</span>
        <span class="Statement">}</span>
      <span class="Statement">}</span>
      </code></pre>

      <h4><code>CHALLENGE</code> Type</h4>
      <p>This message is meant to reply to an <code>EXCHANGE</code> message as the second step in the key exchange process.</p>

      <p>Payload is decrypted with <code>crypto_box_seal_open</code> using the <code>ephemeralKeypair.privateKey</code>.</p>

      <h4>Payload</h4>
      <p>
        <table class="table">
          <tbody>
            <tr>
              <th>Exchange Key</th>
              <td><code>crypto_kx_keypair()</code></td>
            </tr>
            <tr>
              <th>Confirmation Nonce</th>
              <td><code>sodium_increment(client1_payload.meta['auth-nonce'])</code></td>
            </tr>
            <tr>
              <th>Ciphertext</th>
              <td><code>crypto_box_easy(data, confirmationNonce, ephemeralPublicKey, client1.exchangeKeys.publicKey)</code></td>
            </tr>
            <tr>
              <th>Payload</th>
              <td><code>ciphertext</code></td>
            </tr>
          </tbody>
        </table>
      </p>

      <h4>Data</h4>
      <h6>Example challenge response payload</h6>
      <pre class="code" data-lang="JSON:API"><code>
      <span class="Statement">{</span>
        <span class="Constant">&quot;data&quot;</span>: <span class="Statement">[</span>
          <span class="Statement">{</span>
            <span class="Constant">&quot;id&quot;</span>: <span class="Constant">&quot;client0-address&quot;</span>,
            <span class="Constant">&quot;type&quot;</span>: <span class="Constant">&quot;clients&quot;</span>,
            <span class="Constant">&quot;attributes&quot;</span>: <span class="Statement">{</span>
              <span class="Constant">&quot;name&quot;</span>: <span class="Constant">&quot;Bob&quot;</span>,
              <span class="Constant">&quot;exhangeKeys&quot;</span>: <span class="Statement">{</span>
                <span class="Constant">&quot;publicKey&quot;</span>: <span class="Constant">&quot;bada55&quot;</span>
              <span class="Statement">}</span>
            <span class="Statement">}</span>
          <span class="Statement">}</span>,
          <span class="Statement">{</span>
            <span class="Constant">&quot;id&quot;</span>: <span class="Constant">&quot;client1-address&quot;</span>,
            <span class="Constant">&quot;type&quot;</span>: <span class="Constant">&quot;clients&quot;</span>,
            <span class="Constant">&quot;attributes&quot;</span>: <span class="Statement">{</span>
              <span class="Constant">&quot;name&quot;</span>: <span class="Constant">&quot;Alice&quot;</span>,
              <span class="Constant">&quot;exhangeKeys&quot;</span>: <span class="Statement">{</span>
                <span class="Constant">&quot;publicKey&quot;</span>: <span class="Constant">&quot;bada55&quot;</span>
              <span class="Statement">}</span>
            <span class="Statement">}</span>
          <span class="Statement">}</span>,
          <span class="Statement">{</span>
            <span class="Constant">&quot;id&quot;</span>: <span class="Constant">&quot;client2-address&quot;</span>,
            <span class="Constant">&quot;type&quot;</span>: <span class="Constant">&quot;clients&quot;</span>,
            <span class="Constant">&quot;attributes&quot;</span>: <span class="Statement">{</span>
              <span class="Constant">&quot;name&quot;</span>: <span class="Constant">&quot;Charlie&quot;</span>,
              <span class="Constant">&quot;exhangeKeys&quot;</span>: <span class="Statement">{</span>
                <span class="Constant">&quot;publicKey&quot;</span>: <span class="Constant">&quot;bada55&quot;</span>
              <span class="Statement">}</span>
            <span class="Statement">}</span>
          <span class="Statement">}</span>
        <span class="Statement">]</span>
      <span class="Statement">}</span>
      </code></pre>

      <h6>Constructed Socket.io payload</h6>
      <pre class="code" data-lang="JSON"><code>
      <span class="Statement">{</span>
        <span class="Constant">&quot;type&quot;</span>: <span class="Constant">&quot;challenge&quot;</span>,
        <span class="Constant">&quot;from&quot;</span>: <span class="Constant">&quot;client0-address&quot;</span>,
        <span class="Constant">&quot;xdata&quot;</span>: <span class="Statement">{</span>
          <span class="Constant">&quot;client1-address&quot;</span>: <span class="Constant">&quot;encrypted payload&quot;</span>
        <span class="Statement">}</span>
      <span class="Statement">}</span>
      </code></pre>

      <h4><code>DATA</code> Type</h4>
      <div class="toast toast-primary">
        Once both clients have exchanged keys they construct a mutual set of session keys using <code>crypto_kx_client_session_keys</code> and <code>crypto_kx_server_session_keys</code>. The client to <em>send</em> the initial <code>EXCHANGE</code> message is considered the <em>server</em> in this one case.
      </div>

      <p>This message is meant to send any <abbr>JSON:API</abbr> payload to all clients in the room. This will include chat messages as well as a full client list (with exchangeKeys).</p>

      <p>Payload is decrypted with <code>crypto_secretbox_open_easy</code> using the <code>sessionKeys.sharedRx</code>.</p>

      <h4>Payload</h4>
      <p>
        <table class="table">
          <tbody>
            <tr>
              <th>Nonce</th>
              <td><code>randombytes_buf(crypto_secretbox_NONCEBYTES)</code></td>
            </tr>
            <tr>
              <th>Ciphertext</th>
              <td><code>crypto_secretbox_easy(data, nonce, sessionKey.sharedTx)</code></td>
            </tr>
            <tr>
              <th>Payload</th>
              <td><code>nonce + ciphertext</code></td>
            </tr>
          </tbody>
        </table>
      </p>

      <h4>Data</h4>
      <h6>Example chat message</h6>
      <pre class="code" data-lang="JSON:API"><code>
      <span class="Statement">{</span>
        <span class="Constant">&quot;data&quot;</span>: <span class="Statement">{</span>
          <span class="Constant">&quot;id&quot;</span>: <span class="Constant">&quot;datetime-stamp-and-nonce&quot;</span>,
          <span class="Constant">&quot;type&quot;</span>: <span class="Constant">&quot;messages&quot;</span>,
          <span class="Constant">&quot;attributes&quot;</span>: <span class="Statement">{</span>
            <span class="Constant">&quot;text&quot;</span>: <span class="Constant">&quot;Hello World!&quot;</span>
          <span class="Statement">}</span>
        <span class="Statement">}</span>
      <span class="Statement">}</span>
      </code></pre>

      <h6>Example client joined announcement</h6>
      <pre class="code" data-lang="JSON:API"><code>
      <span class="Statement">{</span>
        <span class="Constant">&quot;data&quot;</span>: <span class="Statement">[</span>
          <span class="Statement">{</span>
            <span class="Constant">&quot;id&quot;</span>: <span class="Constant">&quot;client0-address&quot;</span>,
            <span class="Constant">&quot;type&quot;</span>: <span class="Constant">&quot;clients&quot;</span>,
            <span class="Constant">&quot;attributes&quot;</span>: <span class="Statement">{</span>
              <span class="Constant">&quot;name&quot;</span>: <span class="Constant">&quot;Bob&quot;</span>,
              <span class="Constant">&quot;exhangeKeys&quot;</span>: <span class="Statement">{</span>
                <span class="Constant">&quot;publicKey&quot;</span>: <span class="Constant">&quot;bada55&quot;</span>
              <span class="Statement">}</span>
            <span class="Statement">}</span>
          <span class="Statement">}</span>,
          <span class="Statement">{</span>
            <span class="Constant">&quot;id&quot;</span>: <span class="Constant">&quot;client1-address&quot;</span>,
            <span class="Constant">&quot;type&quot;</span>: <span class="Constant">&quot;clients&quot;</span>,
            <span class="Constant">&quot;attributes&quot;</span>: <span class="Statement">{</span>
              <span class="Constant">&quot;name&quot;</span>: <span class="Constant">&quot;Alice&quot;</span>,
              <span class="Constant">&quot;exhangeKeys&quot;</span>: <span class="Statement">{</span>
                <span class="Constant">&quot;publicKey&quot;</span>: <span class="Constant">&quot;bada55&quot;</span>
              <span class="Statement">}</span>
            <span class="Statement">}</span>
          <span class="Statement">}</span>,
          <span class="Statement">{</span>
            <span class="Constant">&quot;id&quot;</span>: <span class="Constant">&quot;client2-address&quot;</span>,
            <span class="Constant">&quot;type&quot;</span>: <span class="Constant">&quot;clients&quot;</span>,
            <span class="Constant">&quot;attributes&quot;</span>: <span class="Statement">{</span>
              <span class="Constant">&quot;name&quot;</span>: <span class="Constant">&quot;Charlie&quot;</span>,
              <span class="Constant">&quot;exhangeKeys&quot;</span>: <span class="Statement">{</span>
                <span class="Constant">&quot;publicKey&quot;</span>: <span class="Constant">&quot;bada55&quot;</span>
              <span class="Statement">}</span>
            <span class="Statement">}</span>
          <span class="Statement">}</span>
        <span class="Statement">]</span>
      <span class="Statement">}</span>
      </code></pre>

      <h6>Constructed Socket.io payload</h6>
      <pre class="code" data-lang="JSON"><code>
      <span class="Statement">{</span>
        <span class="Constant">&quot;type&quot;</span>: <span class="Constant">&quot;challenge&quot;</span>,
        <span class="Constant">&quot;from&quot;</span>: <span class="Constant">&quot;client0-address&quot;</span>,
        <span class="Constant">&quot;xdata&quot;</span>: <span class="Statement">{</span>
          <span class="Constant">&quot;client0-address&quot;</span>: <span class="Constant">&quot;encrypted payload&quot;</span>,
          <span class="Constant">&quot;client1-address&quot;</span>: <span class="Constant">&quot;encrypted payload&quot;</span>,
          <span class="Constant">&quot;client2-address&quot;</span>: <span class="Constant">&quot;encrypted payload&quot;</span>
        <span class="Statement">}</span>
      <span class="Statement">}</span>
      </code></pre>

      <hr>

      <h2>Sequence Diagram</h2>
      <figure class="figure">
        <img class="img-responsive" src="output/client-crypto.svg">
      </figure>
    </section>

    <section id="big-text" class="route">
      <h1 class="badge" data-badge="Experimental">Big Text</h1>

      <p>After some interaction with the deaf community as hearing-enabled I found communication to be difficult. One option that I found worked with good success was the process of typing in a note on a Phone and then showing that to them.</p>
      <p>I used a app called <strong>Big Text</strong> which made the typed message large and with high contrast.</p>
      <p>The disadvantage, however, was that it required me to interrupt the flow by turning my phone around and then offering it to others to respond back. It was a tedious process which would have been alleviated by simply chatting or texting.</p>
      <p>Obviously the <code>instant-chat</code> system is designed to solve this; the system still uses technology and there will be times when it is just not possible to establish a chat between two devices (maybe the other person does not have a device). It is this case having a fallback would allow everyone (even those who can't connect) to have the least amount of resistance to communicating.</p>

      <h2>Example</h2>
      <div class="p-relative">
        <button id="example-big-text-clear" class="btn big-text-clear-btn">Clear</button>
        <div id="example-big-text-wrapper" class="big-text-wrapper">
          <div id="example-big-text-content" class="big-text" contenteditable="true">Example Text</div>
        </div>
      </div>
    </section>

    <section id="one-time-secret" class="route">
      <h1 class="badge" data-badge="Experimental">One Time Secret</h1>

      <p>There could be a situation where someone would want to share some information to another <em>without</em> establishing a chat session. I can see this feature for situations when the user wants to pass along sensitive data like passwords to someone without direct need to interact with them.</p>

      <p>Admittedly this is very out of scope for this project. However, the crypto is already there and the server is so small adding such a feature is almost negligible. As an alternative there is a dedicated application at <a href="https://one-time-secret.com">one-time-secret.com</a> to facilitate this. However, I felt the implementation (specifically the key generation) to be lacking. And wanted to take this on as a fun side project/Easter egg</p>

      <h2>Payload</h2>

      <p>
        <table class="table">
          <tbody>
            <tr>
              <th>Secret Key</th>
              <td><code>crypto_secretbox_key()</code></td>
            </tr>
            <tr>
              <th>Nonce</th>
              <td><code>randombytes_buf(crypto_secretbox_NONCEBYTES)</code></td>
            </tr>
            <tr>
              <th>Ciphertext</th>
              <td><code>crypto_secretbox_easy(data, nonce, key)</code></td>
            </tr>
            <tr>
              <th>Payload</th>
              <td><code>nonce + ciphertext</code></td>
            </tr>
            <tr>
              <th>UUID</th>
              <td><code>crypto_generichash(key, NULL)</code></td>
            </tr>
          </tbody>
        </table>
      </p>

      <h2>HTTP API</h2>

      <h4><code>POST /secrets</code></h4>
      <p>Register a secret.</p>
      <p>Registered links have an <term title="TTL">time to live</term> of <strong>24 hours</strong> by default. Alternative expiration times can be requested via the <code>ttl</code> (in seconds) option.</p>
      <p>The <code>exp</code> on a created response is the expiration datetime (ISO-8601 UTC).</p>
      <pre class="code" data-lang="HTTP"><code>
      &rArr; POST /secrets
      <span class="Statement">{</span>
        <span class="Constant">&quot;uuid&quot;</span>: <span class="Constant">&quot;bada55&quot;</span>,
        <span class="Constant">&quot;ttl&quot;</span>: <span class="Constant">86400</span>,
        <span class="Constant">&quot;payload&quot;</span>: <span class="Constant">&quot;ciphertext&quot;</span>
      <span class="Statement">}</span>

      &lArr; 201 Created
      <span class="Statement">{</span>
        <span class="Constant">&quot;exp&quot;</span>: <span class="Constant">&quot;2018-11-19T14:55:00Z&quot;</span>
      <span class="Statement">}</span>
      </code></pre>

      <h4><code>GET /secrets/:uuid</code></h4>
      <p>Retrieve a secret.</p>
      <div class="toast taost-warning">It is expected that once a secret is fetched that it is immediately removed from knowledge and can no longer be retrieved again.</div>
      <pre class="code" data-lang="HTTP"><code>
      &rArr; GET /secrets/bada55

      &lArr; 200 Ok
      <span class="Statement">{</span>
        <span class="Constant">&quot;payload&quot;</span>: <span class="Constant">&quot;ciphertext&quot;</span>
      <span class="Statement">}</span>

      &lArr; 404 Not Found
      <span class="Statement">{</span>
        <span class="Constant">&quot;error&quot;</span>: <span class="Constant">&quot;Secret not found&quot;</span>
      <span class="Statement">}</span>
      </code></pre>

      <h2>Sequence Diagram</h2>
      <figure class="figure">
        <img class="img-responsive" src="output/one-time-secret.svg">
      </figure>
    </section>

    <footer>
      <a class="btn btn-link route" href="#index">Overview</a> |
      <a class="btn btn-link route" href="#chat-protocol">Chat Protocol</a> |
      <a class="btn btn-link route" href="#client-crypto">Client Crypto</a> |
      <a class="btn btn-link route" href="#big-text">Big Text</a> |
      <a class="btn btn-link route" href="#one-time-secret">One Time Secret</a>
    </footer>
  </div>

  <script>
    document.addEventListener('DOMContentLoaded', function () {
      function transitionCurrentRoute() {
        var href = window.location.hash || '#index';
        var hash = href.substring(1);
        document.querySelectorAll('a.route').forEach(function(link) {
          var method = link.getAttribute('href') === href ? 'add' : 'remove';
          link.classList[method]('active');
        });
        document.querySelectorAll('section.route').forEach(function(section) {
          var method = section.getAttribute('id') === hash ? 'add' : 'remove';
          section.classList[method]('current');
        });
      }
      window.addEventListener('hashchange', transitionCurrentRoute);
      transitionCurrentRoute();

      document
        .getElementById('example-big-text-clear')
        .addEventListener('click', function() {
          document.getElementById('example-big-text-content').innerHTML = '';
        });

      document
        .getElementById('example-big-text-wrapper')
        .addEventListener('click', function() {
          document.getElementById('example-big-text-content').focus();
        });
    });
  </script>
</body>
</html>
