const Router = require('koa-router');
const coRedis = require('co-redis');
const { secretIDFor } = require('../libs/redis-utils');

const SECRET_TTL_TIMES = [
  5 * 60,           // 5 minutes
  60 * 60,          // an hour
  24 * 60 * 60,     // a day
  7 * 24 * 60 * 60, // a week
];

module.exports = function(redisConn) {
  const redis = coRedis(redisConn);
  let router = new Router();

  router.prefix('/api');

  router.post('/secrets', async function(ctx) {
    let { uuid, ttl, payload } = ctx.request.body;
    let key = secretIDFor(uuid);
    ctx.assert(uuid, 422, 'Missing UUID');
    ctx.assert(payload, 422, 'Missing payload');
    ctx.assert(typeof(payload) === 'string', 422, 'payload must be a string');
    ctx.assert(
      SECRET_TTL_TIMES.includes(ttl),
      422,
      `TTL value must be one of ${SECRET_TTL_TIMES.join(', ')}`
    );
    let exp = new Date();
    exp.setSeconds(exp.getSeconds() + ttl);
    try {
      ctx.assert(!await redis.exists(key), 422, 'UUID is not unique');
      await redis.set(key, JSON.stringify(payload));
      await redis.expire(key, ttl);
    } catch (error) {
      ctx.throw(502, error);
    }
    ctx.status = 201;
    ctx.body = { exp: exp.toISOString() };
  });

  router.get('/secrets/:uuid', async function(ctx) {
    let secret;
    let { uuid } = ctx.params;
    let key = secretIDFor(uuid);
    try {
      secret = await redis.get(key);
    } catch (error) {
      ctx.throw(502, error);
    }
    ctx.assert(secret, 410, 'Secret destroyed');
    ctx.body = { payload: JSON.parse(secret) };
  });

  return router;
};
