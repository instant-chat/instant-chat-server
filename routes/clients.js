const Router = require('koa-router');
const coRedis = require('co-redis');
const { clientIDFor } = require('../libs/redis-utils');

const FIVE_MINUTES = 5 * 60;

function isLink(link) {
  return link && /^[A-Z]{4}$/.test(link);
}

function randomLink() {
  return [1, 2, 3, 4]
    .map(() => String.fromCharCode(Math.floor(Math.random() * 25) + 65))
    .join('');
}

async function generateClientLink(redis) {
  let link;
  do { link = randomLink(); }
  while (await redis.exists(clientIDFor(link)));
  return { link, key: clientIDFor(link) };
}

module.exports = function(redisConn) {
  const redis = coRedis(redisConn);
  let router = new Router();

  router.prefix('/api');

  router.post('/clients', async function(ctx) {
    let { body } = ctx.request;
    let { link, key } = await generateClientLink(redis);
    let exp = new Date();
    exp.setSeconds(exp.getSeconds() + FIVE_MINUTES);
    try {
      await redis.set(key, JSON.stringify(body));
      await redis.expire(key, FIVE_MINUTES);
    } catch (error) {
      ctx.throw(502, error);
    }
    ctx.status = 201;
    ctx.body = { link, exp: exp.toISOString() };
  });

  router.get('/clients/:link', async function(ctx) {
    let payload;
    let { link } = ctx.params;
    ctx.assert(isLink(link), 400);
    let key = clientIDFor(link);
    try {
      payload = await redis.get(key);
      if (ctx.method !== 'HEAD') {
        await redis.del(key);
      }
    } catch (error) {
      ctx.throw(502, error);
    }
    ctx.assert(payload, 410, 'Link expired');
    ctx.body = JSON.parse(payload);
  });

  return router;
}
