const Router = require('koa-router');
const coRedis = require('co-redis');
const { revisionIDFor } = require('../libs/redis-utils');

const CURRENT_REVISION_KEY = 'current-content';

function emberRoute(redisConn) {
  const redis = coRedis(redisConn);
  let router = new Router();

  router.get('/*', async function(ctx) {
    let { revision } = ctx.request.query;
    let key = revisionIDFor(revision) || revisionIDFor(CURRENT_REVISION_KEY);
    try {
      ctx.body = await redis.get(key);
    } catch (error) {
      ctx.throw(502, error);
    }
    ctx.assert(ctx.body, 404);
  });

  return router;
}

emberRoute.CURRENT_REVISION_KEY = CURRENT_REVISION_KEY;

module.exports = emberRoute;
