FROM node:latest
ADD . /code
WORKDIR /code
RUN yarn --frozen-lockfile
CMD ["yarn", "start"]
