const expect = require('chai').expect;
const request = require('request-promise');
const redisMock = require('redis-mock');
const coRedis = require('co-redis');
const Server = require('../libs/server');
const { clientIDFor } = require('../libs/redis-utils');

describe('Client registration', function() {
  beforeEach(async function() {
    let logger = (_, next) => next();
    let redis = redisMock.createClient();
    this.request = request.defaults({
      json: true,
      simple: false,
      resolveWithFullResponse: true
    });
    this.redis = coRedis(redis)
    this.server = new Server(redis, logger).listen();
    this.url = `http://127.0.0.1:${this.server.address().port}`;
    await this.redis.set(clientIDFor('ABCD'), '{"foo":"bar"}');
  });

  afterEach(async function() {
    await this.server.close();
    await this.redis.end();
  });

  describe('GET /api/clients/:link', function() {
    it('returns a 400 when link is invalid', async function() {
      let response = await this.request.get(`${this.url}/api/clients/1234`);
      expect(response.statusCode).to.equal(400);
    });

    it('returns a 410 when link does not exist', async function() {
      let response = await this.request.get(`${this.url}/api/clients/WXYZ`);
      expect(response.statusCode).to.equal(410);
    });

    it('returns a payload when link exists', async function() {
      let response = await this.request.get(`${this.url}/api/clients/ABCD`);
      expect(response.statusCode).to.equal(200);
      expect(response.body)
        .to.have.property('foo')
        .and.equal('bar');
      expect(await this.redis.get(clientIDFor('ABCD'))).to.not.exist;
    });

    it('does not delete link when method is HEAD', async function() {
      let response = await this.request.head(`${this.url}/api/clients/ABCD`);
      expect(response.statusCode).to.equal(200);
      expect(await this.redis.get(clientIDFor('ABCD'))).to.exist;
    });
  });

  describe('POST /api/clients', function() {
    it('returns a 201', async function() {
      let body = { foo: 'bar' };
      let response = await this.request.post(`${this.url}/api/clients`, { body });
      expect(response.statusCode).to.equal(201);
      expect(response.body)
        .to.have.property('link')
        .and.match(/^[A-Z]{4}$/);
      expect(response.body)
        .to.have.property('exp')
        .and.is.a('string');
    });
  });
});
