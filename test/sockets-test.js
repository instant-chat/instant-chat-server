const expect = require('chai').expect;
const redisMock = require('redis-mock');
const clientIo = require('socket.io-client');
const Server = require('../libs/server');

function expectIOCallback(done, fn) {
  return function() {
    try { fn(...arguments); }
    catch(error) { return done(error); }
    done();
  };
}

function chain(timeout = 50) {
  let fns = [];
  return {
    next(fn) {
      fns.push(fn);
      return this;
    },
    run() {
      setTimeout(() => {
        let fn = fns.shift();
        if (!fn) { return; }
        fn();
        this.run();
      }, timeout);
    }
  };
}

describe('Socket.io', function() {
  beforeEach(function() {
    let logger = (_, next) => next();
    this.redis = redisMock.createClient();
    this.server = new Server(this.redis, logger).listen();
    this.url = `http://127.0.0.1:${this.server.address().port}`;
  });

  afterEach(function() {
    this.server.close();
    this.redis.end();
  });

  describe('Address Assignment', function() {
    beforeEach(function() {
      this.socket = clientIo(this.url, {
        forceNew: true,
        autoConnect: false,
        transports: ['websocket']
      });
    });

    afterEach(function() {
      this.socket.disconnect();
    });

    it('receives an address on connection', function(done) {
      this.socket.on('address-assignment', expectIOCallback(done, payload => {
        expect(payload).to.have.property('address').and.be.a('string');
      }));
      this.socket.connect();
    });
  });

  describe('Message Relay', function() {
    beforeEach(function() {
      this.socketA = clientIo(this.url, {
        forceNew: true,
        transports: ['websocket']
      });
      this.socketB = clientIo(this.url, {
        forceNew: true,
        transports: ['websocket']
      });
    });

    afterEach(function() {
      this.socketA.disconnect();
      this.socketB.disconnect();
    });

    it('relays messages to clients', function(done) {
      this.socketA.on('message', expectIOCallback(done, () => {
        throw new Error('expected socket A not to receive message event');
      }));
      this.socketB.on('message', expectIOCallback(done, payload => {
        expect(payload).to.have.property('foo').and.equal('bar');
      }));
      chain()
        .next(() => this.socketA.emit('join', { room: 'foobar' }))
        .next(() => this.socketB.emit('join', { room: 'foobar' }))
        .next(() => this.socketA.emit('message', { foo: 'bar' }))
        .run();
    });
  });

  describe('Evictions', function() {
    beforeEach(function() {
      this.socketA = clientIo(this.url, {
        forceNew: true,
        transports: ['websocket']
      });
      this.socketB = clientIo(this.url, {
        forceNew: true,
        transports: ['websocket']
      });
    });

    afterEach(function() {
      this.socketA.disconnect();
    });

    it('emits an eviction event when a client disconnects', function(done) {
      this.socketA.on('eviction', expectIOCallback(done, payload => {
        expect(payload).to.have.property('address').and.is.a('string');
        expect(payload).to.have.property('client', 'clientB');
      }));
      chain()
        .next(() => this.socketA.emit('register-client', { client: 'clientA' }))
        .next(() => this.socketB.emit('register-client', { client: 'clientB' }))
        .next(() => this.socketA.emit('join', { room: 'foobar' }))
        .next(() => this.socketB.emit('join', { room: 'foobar' }))
        .next(() => this.socketB.disconnect())
        .run();
    });
  });
});
