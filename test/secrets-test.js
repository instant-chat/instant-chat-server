const expect = require('chai').expect;
const request = require('request-promise');
const redisMock = require('redis-mock');
const coRedis = require('co-redis');
const Server = require('../libs/server');
const { secretIDFor } = require('../libs/redis-utils');

describe('Secrets', function() {
  beforeEach(async function() {
    let logger = (_, next) => next();
    let redis = redisMock.createClient();
    this.request = request.defaults({
      json: true,
      simple: false,
      resolveWithFullResponse: true
    });
    this.redis = coRedis(redis);
    this.server = new Server(redis, logger).listen();
    this.url = `http://127.0.0.1:${this.server.address().port}`;
    await this.redis.set(secretIDFor('bada55'), '{"foo":"bar"}');
  });

  afterEach(async function() {
    await this.server.close();
    await this.redis.end();
  });

  describe('GET /api/secrets/:uuid', function() {
    it('returns a 410 when secret does not exist', async function() {
      let response = await this.request.get(`${this.url}/api/secrets/not-found`);
      expect(response.statusCode).to.equal(410);
    });

    it('returns a payload when secret exists', async function() {
      let response = await this.request.get(`${this.url}/api/secrets/bada55`);
      expect(response.statusCode).to.equal(200);
      expect(response.body)
        .to.have.property('payload')
        .and.to.have.property('foo')
        .and.equal('bar');
      expect(await this.redis.get(secretIDFor('bada55'))).to.exist;
    });
  });

  describe('POST /api/secrets', function() {
    it('returns a 201 when all params are valid', async function() {
      let body = { uuid: 'unique-id', ttl: 300, payload: 'bada55' };
      let response = await this.request.post(`${this.url}/api/secrets`, { body });
      expect(response.statusCode).to.equal(201);
      expect(response.body)
        .to.have.property('exp')
        .and.match(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/);
    });

    it('returns a 422 when uuid param is missing', async function() {
      let body = { ttl: 300, payload: 'bada55' };
      let response = await this.request.post(`${this.url}/api/secrets`, { body });
      expect(response.statusCode).to.equal(422);
    });

    it('returns a 422 when uuid param is not unique', async function() {
      let body = { uuid: 'bada55', ttl: 300, payload: 'bada55' };
      let response = await this.request.post(`${this.url}/api/secrets`, { body });
      expect(response.statusCode).to.equal(422);
    });

    it('returns a 422 when ttl param is invalid', async function() {
      let body = { uuid: 'unique-id', ttl: 0, payload: 'bada55' };
      let response = await this.request.post(`${this.url}/api/secrets`, { body });
      expect(response.statusCode).to.equal(422);
    });

    it('returns a 422 when payload param is missing', async function() {
      let body = { uuid: 'unique-id', ttl: 300 };
      let response = await this.request.post(`${this.url}/api/secrets`, { body });
      expect(response.statusCode).to.equal(422);
    });

    it('returns a 422 when payload param is invalid', async function() {
      let body = { uuid: 'unique-id', ttl: 300, payload: 12345 };
      let response = await this.request.post(`${this.url}/api/secrets`, { body });
      expect(response.statusCode).to.equal(422);
    });
  });
});
