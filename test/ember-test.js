const expect = require('chai').expect;
const request = require('request-promise');
const redisMock = require('redis-mock');
const coRedis = require('co-redis');
const Server = require('../libs/server');
const { CURRENT_REVISION_KEY } = require('../routes/ember');
const { revisionIDFor } = require('../libs/redis-utils');

describe('Ember lightning deploy routing', function() {
  beforeEach(async function() {
    let logger = (_, next) => next();
    let redis = redisMock.createClient();
    this.request = request.defaults({
      json: true,
      simple: false,
      resolveWithFullResponse: true
    });
    this.redis = coRedis(redis)
    this.server = new Server(redis, logger).listen();
    this.url = `http://127.0.0.1:${this.server.address().port}`;
    this.currentBody = '<html><body>test-current</body></html>';
    this.revisionBody = '<html><body>test-revision</body></html>';
    await this.redis.set(revisionIDFor('bada55'), this.revisionBody);
    await this.redis.set(revisionIDFor(CURRENT_REVISION_KEY), this.currentBody);
  });

  afterEach(async function() {
    await this.server.close();
    await this.redis.end();
  });

  it('it returns 200 for all wild card matches', async function() {
    let response = await this.request.get(`${this.url}/`);
    expect(response.statusCode).to.equal(200);
    expect(response.body).to.equal(this.currentBody);

    response = await this.request.get(`${this.url}/foobar`);
    expect(response.statusCode).to.equal(200);
    expect(response.body).to.equal(this.currentBody);

    response = await this.request.get(`${this.url}/foobar/baz`);
    expect(response.statusCode).to.equal(200);
    expect(response.body).to.equal(this.currentBody);
  });

  it('it returns 200 for known revisions', async function() {
    response = await this.request.get(`${this.url}/foobar?revision=bada55`);
    expect(response.statusCode).to.equal(200);
    expect(response.body).to.equal(this.revisionBody);
  });

  it('it returns 404 for unknown known revisions', async function() {
    response = await this.request.get(`${this.url}/foobar?revision=unknown`);
    expect(response.statusCode).to.equal(404);
  });
});
